#!/bin/sh

set -e
IFS='
'
laptop=no
template_sed_cmds='
s/$TEMPLATE_DUNST_FONT_SIZE/8/g;
s/$TEMPLATE_WM_FONT_SIZE/12/g;
s/$TEMPLATE_BAR_HEIGHT/32/g;
s/$TEMPLATE_BAR_FONT_SIZE/14/g;
s/$TEMPLATE_BAR_FONT_OFFSET/2/g;
s/$TEMPLATE_BAR_ICON_SIZE/15/g;
s/$TEMPLATE_BAR_ICON_OFFSET/5/g;
s/$TEMPLATE_QUTE_DEFAULT_FONT_SIZE/15/g;
s/$TEMPLATE_QUTE_DEFAULT_ZOOM/125/g;
s/$TEMPLATE_ROFI_FONT_SIZE/16/g;
'

if [ -z "$HOME" ]; then
    echo '$HOME is not set, aborting'
    exit 1
fi

usage() {
    cat << EOF
usage: install.sh [options]
OPTIONS:
  -l    Assume laptop (larger fonts); defaults to desktop
EOF
    exit 0
}

while getopts "dhlp" option; do
    case "$option" in
        d) laptop=yes ;;
        ?) usage ;;
    esac
done

if [ "$laptop" = "yes" ]; then
    echo 'Using laptop template values'
    template_sed_cmds='
s/$TEMPLATE_DUNST_FONT_SIZE/10/g;
s/$TEMPLATE_WM_FONT_SIZE/10/g;
s/$TEMPLATE_BAR_HEIGHT/36/g;
s/$TEMPLATE_BAR_FONT_SIZE/18/g;
s/$TEMPLATE_BAR_FONT_OFFSET/3/g;
s/$TEMPLATE_BAR_ICON_SIZE/19/g;
s/$TEMPLATE_BAR_ICON_OFFSET/6/g;
s/$TEMPLATE_QUTE_DEFAULT_FONT_SIZE/16/g;
s/$TEMPLATE_QUTE_DEFAULT_ZOOM/150/g;
s/$TEMPLATE_ROFI_FONT_SIZE/18/g;
'
fi

for source in _*; do
    for dotfile in $(find "$source" -type f | sort); do
        dest="$HOME/.${dotfile#_}"

        # Create parent dirs
        mkdir -p -m 700 "$(dirname "$dest")"

        if [ "$dotfile" != "${dotfile##*.template}" ]; then
            # Is template file, strip *.template suffix and copy
            dest="${dest%.template}"
            echo Installing "$dotfile" to "$dest"

            cp "$(pwd)/$dotfile" "$dest"
            # Substitute template variables
            sed -i "$template_sed_cmds" "$dest"
        else
            # Is normal file, create symlink
            echo Installing "$dotfile" to "$dest"

            ln -f -s "$(pwd)/$dotfile" "$dest"
        fi
    done
done
