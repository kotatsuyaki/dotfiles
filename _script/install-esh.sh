#!/bin/sh

if [ ${EUID:-$(id -u)} -ne 0 ]; then
    echo "Please run as root"
    exit
fi

URL=https://raw.githubusercontent.com/jirutka/esh/v0.3.0/esh
TARGET=/usr/bin/esh

curl "$URL" -o "$TARGET"
chmod +x "$TARGET"
