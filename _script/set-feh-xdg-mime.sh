#!/bin/sh

function setmimes {
    for mime in "$@"; do
        xdg-mime default feh.desktop "$mime"
    done
}

setmimes image/gif image/jpeg image/png image/tiff
