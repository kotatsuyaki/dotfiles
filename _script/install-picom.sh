#!/bin/sh

void_packages="libev-devel xcb-util-renderutil-devel xcb-util-image-devel libconfig-devel ninja meson uthash"
build_dir=/tmp/picom
install_logs_dir="$HOME/.local/install_logs"

rm -rf "$build_dir"
git clone 'https://github.com/yshui/picom.git' "$build_dir"
cd "$build_dir"

echo -n "Install Void Linux dependencies (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    sudo xbps-install $void_packages
fi

echo -n "Install Gentoo Linux dependencies (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    if command -v sudo ; then
        sudo emerge -avq libconfig uthash
    else
        doas emerge -avq libconfig uthash
    fi
fi

# Adapted from 
git submodule update --init --recursive
meson --prefix "$HOME/.local/" --buildtype=release . build
ninja -C build install

mkdir -p -m 700 "$install_logs_dir"
cp ./build/meson-logs/install-log.txt "$install_logs_dir/picom-install-log.txt"
