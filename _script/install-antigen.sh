#!/bin/sh

if ! command -v curl; then
    echo 'curl is needed'; exit 1
fi

curl -L git.io/antigen > ~/.config/antigen.zsh
