#!/bin/sh
# Suspend wrapper script for Runit & OpenRC

if command -v loginctl &> /dev/null; then
    loginctl suspend-then-hibernate
elif command -v zzz &> /dev/null; then
    sudo zzz
fi
