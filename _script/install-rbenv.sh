#!/bin/sh

git clone https://github.com/rbenv/rbenv.git ~/.rbenv
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'Please add this to the shell config file:\nexport PATH="$PATH:$HOME/.rbenv/bin"\neval "$(rbenv init -)"'
