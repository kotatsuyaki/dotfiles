#!/bin/bash
# Source: https://github.com/deiwin/i3-dotfiles/blob/docs/.i3/rename_workspace.sh
# Depends on rofi, jq, i3-msg

REQUIRED="i3-msg jq rofi"

for CMD in $REQUIRED; do
    command -v $CMD 1>/dev/null
    if [ "$?" -ne 0 ]; then
        echo "$CMD not found, aborting"; exit 1
    fi
done

set -e

num=`i3-msg -t get_workspaces | jq 'map(select(.focused == true))[0].num'`
name=$(echo "" | rofi -theme-str 'listview { enabled: false;}' -dmenu -p "New workspace name")
i3-msg "rename workspace to \"$num:$name\""

name=`i3-msg -t get_workspaces | jq 'map(select(.focused == true))[0].name'`
# If empty name was set
if [[ "$name" =~ ^\"[0-9]+:\"$ ]]
then
  # Remove trailing colon and whitespace
  i3-msg "rename workspace to \"$num\""
fi
