" =======================================
" Plugin settings
" =======================================
call plug#begin()
" Intellesence
Plug 'neoclide/coc.nvim', {'branch': 'release'} " LSP client
Plug 'honza/vim-snippets'                       " Snippets support for COC

" Appearance
Plug 'sainnhe/sonokai'     " An improved colorscheme adapted from Monokai
Plug 'ap/vim-buftabline'   " Make buffers show as tabs on top
Plug 'chrisbra/Colorizer'  " Highlight color codes
Plug 'Yggdroot/indentLine' " Vertical lines for indentation hint
Plug 'https://gitlab.com/Akitaki/vim-solarized8.git' " Solarized, customized

" Basic
Plug 'tpope/vim-surround'      " Quickly edit brackets
Plug 'jiangmiao/auto-pairs'    " Auto closing brackets
Plug 'junegunn/vim-easy-align' " Alignment
Plug 'preservim/nerdcommenter' " Comment toggle

" Git
Plug 'airblade/vim-gitgutter'         " Git diff in left column
Plug 'itchyny/vim-gitbranch'          " Provide git branch info for statusline
Plug 'airblade/vim-rooter'            " Auto-switch pwd to git root
Plug 'tpope/vim-fugitive'             " The Git plugin
Plug 'tommcdo/vim-fugitive-blame-ext' " Show commit msg in Gblame

" Syntax
Plug 'rust-lang/rust.vim'        " Rust integration
Plug 'cespare/vim-toml'          " TOML syntax
Plug 'godlygeek/tabular'         " Dependency of vim-markdown
Plug 'plasticboy/vim-markdown'   " Better markdown syntax
Plug 'tikhomirov/vim-glsl'       " GLSL syntax highlight
Plug 'dart-lang/dart-vim-plugin' " Dart lang syntax
Plug 'pangloss/vim-javascript'   " Better js syntax
Plug 'MaxMEllon/vim-jsx-pretty'  " Better jsx syntax

" Other integrations
Plug 'francoiscabrol/ranger.vim'  " Integration with ranger file manager
Plug 'dhruvasagar/vim-table-mode' " Markdown style table
Plug 'kana/vim-textobj-user'      " Custom text objects
Plug 'rbonvall/vim-textobj-latex' " Latex text objects
Plug 'junegunn/goyo.vim'          " Distraction-free writing mode
Plug 'mattn/emmet-vim'            " Emmet

" nvim-specific fixes
if has('nvim')
    Plug 'rbgrouleff/bclose.vim' " Dependency for ranger.vim
endif
call plug#end()

" =======================================
" Coc extension list
" =======================================
let g:coc_global_extensions = [
            \ 'coc-html',
            \ 'coc-css',
            \ 'coc-tsserver',
            \ 'coc-prettier',
            \ 'coc-json',
            \ 'coc-rust-analyzer',
            \ 'coc-clangd',
            \ 'coc-vimlsp',
            \ 'coc-sh',
            \ 'coc-pyright'
            \ ]

" =======================================
" Basic settings
" =======================================
set nocp nu rnu ch=2 sts=4 ts=4 sw=4 ic scs hid ai si hls is et mouse=a
set fdm=syntax foldlevel=99

" Set some filetype's indent width to 2 spaces
au FileType html,json,yaml,css,javascript set ts=2 sw=2 sts=2 et
au FileType json let g:indentLine_enabled = 0

" Show ruler on filetype gitcommit
au FileType gitcommit setlocal tw=72

" Default to LaTeX instead of TeX
au BufRead,BufNewFile,BufEnter *.tex setlocal ft=tex

" Toggle inlay hints in Rust files
autocmd FileType rust nnoremap <silent> <buffer> <leader>n :CocCommand rust-analyzer.toggleInlayHints<CR>

" Make using commands easier
nnoremap ; :
vnoremap ; :

" Quickly insert semicolon
nnoremap <Leader>; A;
inoremap <Leader>; <Esc>A;

" Use t? to manipulate buffers quickly
nnoremap <silent> tp :bp<CR>
nnoremap <silent> tn :bn<CR>
nnoremap <silent> td :bd<CR>
nnoremap tt :e<Space>

" Use '//' to search for visually selected text
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>

" Press enter to dismiss search highlight
nnoremap <silent> <CR> :noh<CR><CR>

" Ctrl-P for COC file list
nnoremap <silent> <C-P> :CocList files

" Enable true color
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" Set colorscheme
colorscheme sonokai

" Source coc-specific settings
source ~/.cocrc

" Change inlay hint style to that of a comment
highlight! link CocRustChainingHint Comment

filetype plugin indent on
syntax on

" =======================================
" Save active theme (dark/light) into file
" =======================================
let g:themefile = (exists('*stdpath') ? (stdpath('data') . '/savedtheme') : '~/.vim_savedtheme')
if filereadable(g:themefile)
    let g:SAVED_THEME = readfile(g:themefile)[0]
else
    let g:SAVED_THEME = 'dark'
endif

function! Setdarktheme()
    set bg=dark | colorscheme sonokai | hi link BufTabLineCurrent Visual
    call writefile(['dark'], g:themefile)
endfunction

function! Setlighttheme()
    set bg=light | colorscheme solarized8_high
    hi link CocRustTypeHint Comment
    call writefile(['light'], g:themefile)
endfunction

if g:SAVED_THEME == 'dark'
    call Setdarktheme()
elseif g:SAVED_THEME == 'light'
    call Setlighttheme()
endif

" =======================================
" fcitx auto on/off support
" =======================================
" Toggle Fcitx
command! ImeOff silent call system('fcitx-remote -c')
command! ImeOn silent call system('fcitx-remote -o')

function! ImeAutoOff()
    let w:ime_status=system('~/.script/.im-stat.sh')
    :silent ImeOff
endfunction

function! ImeAutoOn()
    if !exists('w:ime_status')
        let w:ime_status=0
    endif
    if w:ime_status==1
        :silent ImeOn
    endif
endfunction

" IME off when in insert mode
augroup InsertHook
    autocmd!
    autocmd InsertLeave * call ImeAutoOff()
    autocmd InsertEnter * call ImeAutoOn()
augroup END

" =======================================
" Neovim-specific basic settings
" =======================================
au TermOpen * IndentLinesDisable
au FileType markdown,help,tex IndentLinesDisable

" =======================================
" Statusline settings
" =======================================
" Source: https://shapeshed.com/vim-statuslines/#showing-the-statusline
function! StatuslineGit()
    let l:branchname = gitbranch#name()
    " If the branch name 's length is positive, wrap it with brackets
    return strlen(l:branchname) > 0 ? '  ['.l:branchname.'] ' : ''
endfunction

function! StatusDiagnostic() abort
  let info = get(b:, 'coc_diagnostic_info', {})
  if empty(info) | return '' | endif
  let msgs = []
  if get(info, 'error', 0)
    call add(msgs, 'E' . info['error'])
  endif
  if get(info, 'warning', 0)
    call add(msgs, 'W' . info['warning'])
  endif
  return join(msgs, ' '). ' ' . get(g:, 'coc_status', '')
endfunction

function! StatusIMELock()
  if exists('w:ime_status') && w:ime_status==1
    return '<Fcitx Lock>'
  endif
  return ''
endfunction

" Always show statusline
set laststatus=2
" Clear statusline
set statusline=
" Git branch name
set statusline+=%#Visual#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
" Filename
set statusline+=\ %f\ 
" Readonly ([RO]) and modify flag ([+])
set statusline+=%r%m\ 
" Spacing
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=%{StatusDiagnostic()}\ 
set statusline+=\ %{StatusIMELock()}
" Filetype
set statusline+=\ %y
" Encoding
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
" Format
set statusline+=\[%{&fileformat}\]
" Scroll percentage
set statusline+=\ %p%%
" Cursor position
set statusline+=\ %l:%c
" Padding space
set statusline+=\ 

" =======================================
" GUI settings
" =======================================
if has("gui_running")
    " Use Fira Code font
    set guifont=Fira\ Code\ 12
    " Disable toolbar
    set guioptions-=T
endif

" =======================================
" Custom commands
" =======================================
command Reload so ~/.vimrc
command LightTheme call Setlighttheme()
command DarkTheme call Setdarktheme()

" =======================================
" (plugin) ap/vim-buftabline settings
" =======================================
hi link BufTabLineCurrent Visual
set hidden

" =======================================
" (plugin) junegunn/vim-easy-align settings
" =======================================
nmap <Leader>ga <Plug>(EasyAlign)
xmap <Leader>ga <Plug>(EasyAlign)

" =======================================
" (plugin) francoiscabrol/ranger.vim settings
" =======================================
let g:ranger_replace_netrw = 1

" =======================================
" (plugin) preservim/nerdcommenter
" =======================================
let g:NERDSpaceDelims = 1

" =======================================
" (plugin) airblade/vim-rooter
" =======================================
let g:rooter_patterns = [ '.git' ]

" =======================================
" (plugin) alerque/vim-commonmark
" =======================================
let g:commonmark#extentions#all = 1
