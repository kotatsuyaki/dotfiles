# vim: set ft=zsh:

readonly ANTIGEN_PATH=~/.config/antigen.zsh
readonly LOCAL_ZSHRC_PATH=~/.localzshrc
readonly PROMPT_ORIGINAL='%{$fg[$user_color]%}%n %{$reset_color%}$(_fishy_collapsed_wd)%{$reset_color%}%(!.#.>) '
readonly PROMPT_NEW='%{$fg[$user_color]%}%n %{$reset_color%}@%M $(_fishy_collapsed_wd)%{$reset_color%}%(!.#.>) '

if [ -f "$ANTIGEN_PATH" ]; then
    source ~/.config/antigen.zsh
    # Basic functions
    antigen bundle zsh-users/zsh-autosuggestions
    antigen bundle zsh-users/zsh-syntax-highlighting

    # FZF integration
    antigen bundle unixorn/fzf-zsh-plugin

    # Fish theme
    antigen theme sudorook/fishy-lite themes/fishy
    antigen bundle sudorook/fishy-lite plugins/git

    # Notification for long-running commands
    antigen bundle marzocchi/zsh-notify

    antigen apply

    # Ensure that we're merely adding "@M" (hostname) to the prompt, instead of
    # overwriting the whole styling.
    if [ "$PROMPT" != "$PROMPT_ORIGINAL" ]; then
        echo "Prompt doesn't match our assertion; please check fishy-lite repo."
    fi
    # Add "@M" to the prompt.
    PROMPT="$PROMPT_NEW"
else
    echo "antigen not installed"
fi

if [ -f "$LOCAL_ZSHRC_PATH" ]; then
    source "$LOCAL_ZSHRC_PATH"
fi

# Run ranger using Ctrl+N.
# ^n = Ctrl+N
# ^u = Ctrl+U
# ^M = Return
if command -v ranger &> /dev/null; then
    bindkey -s '^n' '^uranger^M'
fi

source "$HOME/.config/zsh-support/fzf-key-bindings.zsh"
if [ -f "$HOME/.cargo/env" ]; then
    source "$HOME/.cargo/env"
fi
