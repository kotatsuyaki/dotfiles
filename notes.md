Misc Notes for Setting up Systems
=================================

## Xorg Cursor Theme

1. Find path to the cursor theme (not icon theme).  For example, on Gentoo Linux
   , cursor theme of Adwaita is located in

   ```
   /usr/share/cursors/xorg-x11/Adwaita/cursors
   ```

2. Create `~/.icons/default` directory
3. Create symlink.

   ```
   ln -s /usr/share/cursors/xorg-x11/Adwaita/cursors ~/.icons/default/cursors
   ```
